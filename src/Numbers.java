import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Let the game begin!");
        String name = "";

        while (name.isEmpty() || !name.matches("[a-zA-Z]+")) {
            System.out.println("Enter your name: ");
            name = scanner.nextLine();

            if (!name.matches("[a-zA-Z]+")) {
                System.err.println("Invalid name. Please enter a valid name containing only letters.");
            }
        }

        Object[][] yearsAndEvents = {
                {1939, "World War II"},
                {1969, "First Moon Landing"},
                {2001, "September 11 Attacks"},
                {1989, "Fall of the Berlin Wall"},
                {1945, "End of World War II"}
        };

        int index = random.nextInt(yearsAndEvents.length);
        int secretYear = (int)yearsAndEvents[index][0];
        String event = (String) yearsAndEvents[index][1];

        System.out.println("When did the " + event + " begin?");

        int[] guesses = new int[100];
        int guessCount = 0;

        while (true) {
            System.out.println("Enter your guess: ");
            while (!scanner.hasNextInt()) {
                System.err.println("This is not a number!");
                System.out.print("Enter your guess: ");
                scanner.next();
            }

            int guess = scanner.nextInt();
            guesses[guessCount] = guess;
            guessCount++;

            if (guess < secretYear) {
                System.out.println("Your guess is too small. Please, try again.");
            } else if (guess > secretYear) {
                System.out.println("Your guess is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s!%n", name);
                break;
            }
        }

        System.out.println("Your numbers:");
        int[] sortedGuesses = Arrays.copyOf(guesses, guessCount);
        Arrays.sort(sortedGuesses);
        for (int i = guessCount - 1; i >= 0; i--) {
            System.out.println(sortedGuesses[i]);
        }
    }
}
